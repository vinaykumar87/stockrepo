package com.epam.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.dto.CheckPriceOf;
import com.epam.service.StockPushService;

//@RequestMapping("/trade")
@Controller
public class WelcomeController {

	@Autowired
	StockPushService stockPushService;

	// inject via application.properties
	@Value("${welcome.message}")
	private String message;

	private List<String> tasks = Arrays.asList("a", "b", "c", "d", "e", "f", "g");

	@GetMapping("/")
	public String main(Model model) {
		model.addAttribute("message", message);
		model.addAttribute("tasks", tasks);

		return "welcome"; // view
	}

	// /hello?name=kotlin
	@GetMapping("/hello")
	public String mainWithParam(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model) {

		model.addAttribute("message", name);

		return "welcome"; // view
	}

	// public ResponseEntity<CheckPriceOf> process(@RequestBody String req){
	// @PostMapping(value="/process" ,consumes = {MimeTypeUtils.TEXT_PLAIN_VALUE})
	@GetMapping(value = "/process")
	public ResponseEntity<List<CheckPriceOf>> process() {
		List<CheckPriceOf> rsp = new ArrayList<CheckPriceOf>();
		CheckPriceOf apple = new CheckPriceOf();
		apple.setAskPrice(Math.floor(Math.random() * 1000));
		apple.setBidPrice(Math.floor(Math.random() * 1000));
		apple.setCompany("Apple");
		apple.setCode("AP");
		
		CheckPriceOf microsoft = new CheckPriceOf();
		microsoft.setAskPrice(Math.floor(Math.random() * 1000));
		microsoft.setBidPrice(Math.floor(Math.random() * 1000));
		microsoft.setCompany("Microsoft");
		microsoft.setCode("MS");
		
		CheckPriceOf facebook = new CheckPriceOf();
		facebook.setAskPrice(Math.floor(Math.random() * 1000));
		facebook.setBidPrice(Math.floor(Math.random() * 1000));
		facebook.setCompany("Facebook");
		facebook.setCode("FB");
		
		CheckPriceOf epam = new CheckPriceOf();
		epam.setAskPrice(Math.floor(Math.random() * 1000));
		epam.setBidPrice(Math.floor(Math.random() * 1000));
		epam.setCompany("EPAM");
		epam.setCode("EP");
		
		rsp.add(epam);
		rsp.add(microsoft);
		rsp.add(apple);
		rsp.add(facebook);

		// rsp.setRandomPrice(stockPushService.pushNotification());
		System.out.print("process...123");
		HttpStatus status = HttpStatus.OK;
		return new ResponseEntity<>(rsp, status);
	}

}
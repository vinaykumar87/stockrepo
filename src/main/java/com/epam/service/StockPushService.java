package com.epam.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class StockPushService {
	
	@Scheduled(cron = "${push_time}")
	public double pushNotification() {
		double random = Math.random();
		System.out.println(random);
		return random;
	}
	
}
